#/bin/sh
SETUP_HOME=`pwd`
echo "################################"
echo "# 事前設定"
echo "################################"
read -sp "tebine's bitbucket password?: " BITBUCKET_PASS


echo ""
echo "################################"
echo " Install python"
echo "################################"
sudo pyenv install anaconda3-4.2.0
sudo pyenv global anaconda3-4.2.0
sudo -H pip install --upgrade pip

echo "################################"
echo " Install uWSGI"
echo "################################"
sudo -H pip install uwsgi
sudo mkdir -p /etc/uwsgi/vassals
sudo chown www-data:www-data -R /etc/uwsgi
sudo mkdir -p /var/log/uwsgi/
sudo chown www-data:www-data -R /var/log/uwsgi
sudo mkdir -p /var/www/
sudo chown www-data:www-data -R /var/www

echo "################################"
echo " Download project from bitbucket"
echo "################################"
cd /var/www
sudo git clone https://tebine:$BITBUCKET_PASS@bitbucket.org/9dw/surtr.git
sudo chown -R www-data.www-data surtr/
cd surtr
sudo git checkout -b develop origin/develop

echo "################################"
echo " Add to Python package"
echo "################################"
cd /var/www
PY_SITE_PACKAGE_PATH=/usr/local/pyenv/versions/anaconda3-4.2.0/lib/python3.5/site-packages
sudo ln -si `pwd`/surtr ${PY_SITE_PACKAGE_PATH}/surtr

echo "################################"
echo " Add modules"
echo "################################"
cd surtr
sudo pip install -r requirements.txt
sudo conda install -y -c conda-forge tensorflow=1.4
sudo conda install -y -c menpo opencv3
sudo apt-get install -y libgtk2.0-0

echo "################################"
echo " Change TimeZone"
echo "################################"
sudo cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

echo "################################"
echo " Nginx Setting"
echo "################################"
sudo cp $SETUP_HOME/config/nginx/surtr.conf /etc/nginx/conf.d/surtr.conf
sudo cp $SETUP_HOME/config/nginx/nginx.conf /etc/nginx/nginx.conf

echo "################################"
echo " Kernel parameter tuning"
echo "################################"
sudo sh -c "echo 'net.core.somaxconn = 4096' >> /etc/sysctl.conf"
sudo sysctl -w net.core.somaxconn=4096

echo "################################"
echo " uWSGI settings"
echo "################################"
sudo cp $SETUP_HOME/config/uwsgi/emperor.ini /etc/uwsgi/emperor.ini
sudo cp $SETUP_HOME/config/uwsgi/uwugi.service /etc/systemd/system/uwsgi.service
sudo systemctl enable uwsgi.service
sudo systemctl start uwsgi.service
sudo cp $SETUP_HOME/config/uwsgi/surtr.ini /etc/uwsgi/vassals/surtr.ini

echo "################################"
echo " Modify Tensorflow"
echo "################################"
sudo cp $SETUP_HOME/config/tensorflow/session.py /usr/local/pyenv/versions/anaconda3-4.2.0/lib/python3.5/site-packages/tensorflow/python/client/session.py

echo "################################"
echo " cron setting"
echo "################################"
sudo crontab -u tebine $SETUP_HOME/config/cron/cron

echo "################################"
echo " restart nginx"
echo "################################"
sudo systemctl restart nginx.service
