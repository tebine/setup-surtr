# setup-surtr

----
## これは何？
群馬大PJの細胞分類アプリをセットアップする作業を、ある程度自動化させたスクリプトです。  

## 使い方

こちらも見てね → [サーバーセットアップ手順](https://9dworks.atlassian.net/wiki/spaces/GUCC/pages/671481873)


### サーバーセットアップ用
step1.sh  
step2.sh  
の順番に起動していきます。  
必要な値は、シェルを起動した直後に聞かれるので、入力してください。

### MySQLのBLOB Path書き換え用
mysqlReplace.sh  
を起動します。
