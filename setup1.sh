#/bin/sh

# パスワードなしsudoだったのでこの記法はいらなかった 
#read -sp "Password: " pass
#echo $pass | sudo apt-get install language-pack-ja

################################
# 参考 https://9dworks.atlassian.net/wiki/x/PYChCQ
################################
echo "################################"
echo "# 事前設定"
echo "################################"

# MySQL
read -sp "MySQL root Password?: " PASS
echo "mysql-server mysql-server/root_password password $PASS" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $PASS" | sudo debconf-set-selections

echo ""
echo "################################"
echo "# Set Locale"
echo "################################"
sudo apt-get install -y language-pack-ja zip unzip
sudo update-locale LANG=ja_JP.UTF-8

echo "################################"
echo "# Update packages"
echo "################################"
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

echo "################################"
echo " Install ntp"
echo "################################"
sudo apt-get install -y ntp
sudo sh -c "echo 'server ntp.nict.jp' >> /etc/ntp.conf"

echo "################################"
echo " Install MySQL"
echo "################################"
sudo apt-get install -y mysql-server mysql-client
sudo systemctl enable mysql.service
sudo systemctl start mysql.service

echo "################################"
echo " Insall nginx"
echo "################################"
curl http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
sudo sh -c "echo 'deb http://nginx.org/packages/ubuntu/ xenial nginx' >> /etc/apt/sources.list"
sudo sh -c "echo 'deb-src http://nginx.org/packages/ubuntu/ xenial nginx' >> /etc/apt/sources.list"
sudo apt-get install -y nginx

echo "################################"
echo " Install pyenv"
echo "################################"
sudo apt-get install -y git gcc make openssl libssl-dev libbz2-dev libreadline-dev libsqlite3-dev libsm6 libxext6 libxrender-dev
cd /usr/local/
sudo git clone git://github.com/yyuu/pyenv.git ./pyenv
sudo mkdir -p ./pyenv/versions ./pyenv/shims
cd /usr/local/pyenv/plugins/
sudo git clone git://github.com/yyuu/pyenv-virtualenv.git
sudo git clone git://github.com/yyuu/pyenv-pip-rehash.git
sudo echo 'export PYENV_ROOT="/usr/local/pyenv"' | sudo tee -a /etc/profile.d/pyenv.sh
sudo echo 'export PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"' | sudo tee -a /etc/profile.d/pyenv.sh
source /etc/profile.d/pyenv.sh

# visudoのエディタをvimへ変更 
sudo update-alternatives --set editor /usr/bin/vim.basic

echo ""
echo "sudoでpyenvを実行するのにPATHを引き継ぎたいが、"
echo "どうしても visudo で設定ファイルを変更する以外の方法が "
echo "見つからなかったので、ここは手で設定する。"
echo ""
echo "sudo visudo"
echo "以下はコメントアウトする "
echo "Defaults       env_reset"
echo "Defaults       secure_path=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sb"
echo "以下を書き加える"
echo 'Defaults env_keep += "PATH"'
echo 'Defaults env_keep += "PYENV_ROOT"'
echo ""
echo "書き加えたらログアウト、ログインが必要！！ "
