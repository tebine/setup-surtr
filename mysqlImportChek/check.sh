#/bin/sh
read -sp "MySQL pass?: " MYSQL_PASS

echo -e "\n################################"
echo -e "# 件数確認"
echo -e "################################"
mysql -uroot -p$MYSQL_PASS < ./sql/dbImportCheck.sql > tmp.txt
cat tmp.txt|tr '\n' ',' |sed -e 's/,[a-z]/\n/g'
rm tmp.txt

echo -e "\n################################"
echo -e "# オートインクリメント確認"
echo -e "################################"
mysql -uroot -p$MYSQL_PASS < ./sql/autoIncCheck.sql > tmp.txt
cat tmp.txt|grep -e Name -e Auto_increment
rm tmp.txt
